from django.template import loader
import requests
import landingpage.landingpageapp.handle.genericparser as parser
from landingpage.landingpageapp.handle import retrieval
import errorpages

def get_html_string(request, prefix, suffix):
    try:
        json_record = retrieval.get_handle_record_json(prefix+'/'+suffix)
    except ValueError as e:
        return errorpages.problem_page(request, prefix+'/'+suffix, e.message)

    context = _get_context(json_record)
    template = _get_template()

    return template.render(context, request)


def _get_template():
    return loader.get_template('landingpage/cmip6_dataset.html')

def _get_context(json_record):

    context = {}

    context['content'] = json_record
    context['handle'] = json_record['handle']
    context['values'] = parser.get_all_entries_as_dict(json_record)
    return context
