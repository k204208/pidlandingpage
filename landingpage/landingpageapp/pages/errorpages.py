from django.template import loader
import requests

def no_handle_specified(request):
    template = loader.get_template('landingpage/nohandle.html')
    context = {}
    return template.render(context, request)


def only_prefix(request, prefix):
    template = loader.get_template('landingpage/prefix_only.html')
    context = {
        "prefix": prefix
    }
    return template.render(context, request)

def only_prefix_cmip6(request, prefix):
    template = loader.get_template('landingpage/cmip6_prefix_only.html')
    context = {
        "prefix": prefix
    }
    return template.render(context, request)

def problem_page_cmip6(request, handle, msg):
    template = loader.get_template('landingpage/cmip6_problempage.html')
    context = {
        "handle": handle,
        "msg": msg
    }
    return template.render(context, request)

def problem_page(request, handle, msg):
    template = loader.get_template('landingpage/problempage_dkrz.html')
    context = {
        "handle": handle,
        "msg": msg
    }
    return template.render(context, request)