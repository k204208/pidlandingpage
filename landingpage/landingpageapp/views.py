from django.shortcuts import render
from django.http import HttpResponse
import pages.cmip6 as cmip6
from pages import errorpages, default
from pages.cmip6 import entrypoint

def prefix_only(request, prefix):
    string = errorpages.only_prefix(request, prefix)
    return HttpResponse(string)

def entire_handle(request, prefix, suffix):
    string = default.get_html_string(request, prefix, suffix)
    return HttpResponse(string)

def prefix_only_cmip6(request, prefix):
    string = errorpages.only_prefix_cmip6(request, prefix)
    return HttpResponse(string)

def entire_handle_cmip6(request, prefix, suffix):
    try:
        string = cmip6.entrypoint.get_html_string(request, prefix, suffix)
        return HttpResponse(string)
    except ValueError as e:
        return problem_page_cmip6(request, prefix+'/'+suffix, e.message)

def no_handle(request):
    string = errorpages.no_handle_specified(request)
    return HttpResponse(string)

def problem_page(request, handle, msg=None):
    string = errorpages.problem_page(request, handle, msg)
    return HttpResponse(string)

def problem_page_cmip6(request, handle, msg=None):
    string = errorpages.problem_page_cmip6(request, handle, msg)
    return HttpResponse(string)

# TODO: Allowed methods! --> https://docs.djangoproject.com/en/1.9/topics/http/decorators/