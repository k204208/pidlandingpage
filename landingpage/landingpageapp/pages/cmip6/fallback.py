from django.template import loader
import xml.etree.ElementTree as ET
import landingpage.landingpageapp.handle.cmip6parser as parser

#
# API of this module:
# 

def get_context(json_record):
    return _get_context(json_record)
 
def get_template():
    return _get_template()

def _get_template():
    return loader.get_template('landingpage/cmip6_fallback.html')

#
# Logic of this module:
#

def _get_context(json_record):

    context = {}

    context['content'] = json_record
    context['handle'] = json_record['handle']
    context['agg_level'] = parser._get_aggregation_level(json_record)
    context['values'] = parser.get_all_entries_as_dict(json_record)
    return context
