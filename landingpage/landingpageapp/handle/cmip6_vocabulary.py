
# Keys that occur in CMIP6 handle records:

KEYS = dict(
    agg_level = 'AGGREGATION_LEVEL',
    superpart = 'IS_PART_OF',
    errata_ids = 'ERRATA_IDS',
    file_name = 'FILE_NAME',
    file_size = 'FILE_SIZE',
    checksum = 'CHECKSUM',
    checksum_method = 'CHECKSUM_METHOD',
    url_orig = 'URL_ORIGINAL_DATA',
    url_replica = 'URL_REPLICA',
    subparts = 'HAS_PARTS',
    replica_nodes = 'REPLICA_NODE',
    data_node = 'HOSTING_NODE',
    drs_id = 'DRS_ID',
    vers_num = 'VERSION_NUMBER',
    replaced_by = 'REPLACED_BY',
    replaces = 'REPLACES'

)

# List separator

LIST_SEPARATOR = ';'

# Controlled vocabularies:

LEVELS = dict(
    file = 'FILE',
    dataset = 'DATASET'
)

XML_ATTRIBUTES = dict(
    location = 'location',
    host = 'host',
    href = 'href'

)