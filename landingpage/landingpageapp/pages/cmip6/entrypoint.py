from django.template import loader
import requests
import json
import xml.etree.ElementTree as ET
import filelevel, datasetlevel, fallback
from landingpage.landingpageapp.handle import retrieval, cmip6parser


def get_html_string(request, prefix, suffix):
    prefix = _remove_hdl_from_prefix(prefix)
    json_record = retrieval.get_handle_record_json(prefix+'/'+suffix)
    context = _get_context_for_aggregation_level(json_record)
    template = _get_template_for_aggregation_level(json_record)

    return template.render(context, request)

def _get_template_for_aggregation_level(json_record):
    if cmip6parser.is_file(json_record):
        return filelevel.get_template()
    elif cmip6parser.is_dataset(json_record):
        return datasetlevel.get_template()
    else:
        return fallback.get_template()


def _get_context_for_aggregation_level(json_record):
    if cmip6parser.is_file(json_record):
        return filelevel.get_context(json_record)
    elif cmip6parser.is_dataset(json_record):
        return datasetlevel.get_context(json_record)
    else:
        return fallback.get_context(json_record)


def _get_handle_info_for_dataset(values):
    return {}

def _remove_hdl_from_prefix(prefix):
    if prefix.startswith('hdl:'):
        return prefix.replace('hdl:','')
    else:
        return prefix