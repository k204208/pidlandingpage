import xml.etree.ElementTree as ET
from landingpage.landingpageapp.config_and_defaults import DEFAULTS
import retrieval

###
### API
###

# Parsing any cmip6 handle records

def get_all_entries_as_dict(record, include_special_types=True):
    return _get_all_entries_as_dict(record, include_special_types)

def _get_all_entries_as_dict(record, include_special_types=True):
    special_types = DEFAULTS['special_types']
    record_dict = {}
    for entry in record['values']:
        key = entry['type']
        if key in special_types and not include_special_types:
            pass
        else:
            record_dict[key] = str(get_value_from_record(record, key))
    return record_dict