import json
import logging
LOGGER = logging.getLogger(__name__)
LOGGER.addHandler(logging.NullHandler())

try:
    CONFIG = json.loads(open('./landingpage/landingpageapp/config.json').read())
except IOError:
    LOGGER.info('No config found.')
    CONFIG = {}

DEFAULTS = {}
DEFAULTS['globalresolver'] = 'http://hdl.handle.net'
DEFAULTS['path_restapi'] = 'api/handles'
DEFAULTS['special_types'] = ['HS_ADMIN', 'HS_VLIST', 'HS_PUBKEY', 'HS_SECKEY']

