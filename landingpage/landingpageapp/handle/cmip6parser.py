import xml.etree.ElementTree as ET
from landingpage.landingpageapp.config_and_defaults import DEFAULTS
from landingpage.landingpageapp.handle.cmip6_vocabulary import KEYS as KEYS
from landingpage.landingpageapp.handle.cmip6_vocabulary import LEVELS as LEVELS
from landingpage.landingpageapp.handle.cmip6_vocabulary import LIST_SEPARATOR as SEPARATOR
from landingpage.landingpageapp.handle.cmip6_vocabulary import XML_ATTRIBUTES as XML_ATTRIBUTES
import retrieval

###
### API
###

# Parsing any cmip6 handle records

def get_all_entries_as_dict(record, include_special_types=True):
    return _get_all_entries_as_dict(record, include_special_types)

def get_aggregation_level(record):
    return _get_aggregation_level(record)

def is_file(record):
    return _is_file(record)

def is_dataset(record):
    return _is_dataset(record)

def get_title_from_record(record):
    return _get_title_from_record(record)

def get_aggregation_field_from_record(record):
    return _get_aggregation_field_from_record(record)

# File handle records

def get_filename_from_record(record):
    return _get_filename_from_record(record)

def get_filesize_from_record(record):
    return _get_filesize_from_record(record)

def get_checksum_from_record(record):
    return _get_checksum_from_record(record)

def get_checksum_method_from_record(record):
    return _get_checksum_method_from_record(record)

def get_urls_original_from_record(record):
    return _get_urls_original_from_record(record)

def get_urls_replicas_from_record(record):
    return _get_urls_replicas_from_record(record)

def get_list_of_aggregation_handles_from_record(record):
    return _get_list_of_aggregation_handles_from_record(record)

def get_list_of_aggregation_records_from_record(record):
    return _get_list_of_aggregation_records_from_record(record)

# Dataset handle records

def get_drs_name_from_record(record):
    return _get_drs_name_from_record(record)

def get_version_number_from_record(record):
    return _get_version_number_from_record(record)

def get_replaced_by_from_record(record):
    return _get_replaced_by_from_record(record)

def get_replaces_from_record(record):
    return _get_replaces_from_record(record)

def get_hosts_original_from_record(record):
    return _get_hosts_original_from_record(record)

def get_hosts_replicas_from_record(record):
    return _get_hosts_replicas_from_record(record)

def get_list_of_parts_handles_from_record(record):
    return _get_list_of_parts_handles_from_record(record)

def get_list_of_parts_records_from_record(record):
    return _get_list_of_parts_records_from_record(record)

def get_errata_list_from_record(record):
    return _get_errata_list_from_record(record)

def get_errata_field_from_record(record):
    return _get_errata_field_from_record(record)

# Actual implementation and helpers

def _get_title_from_record(record):
    if is_file(record):
        return get_filename_from_record(record)
    elif is_dataset(record):
        return get_drs_from_record(record)
    else:
        return None

def _get_aggregation_level(record):
    return get_value_from_record(record, KEYS['agg_level'])

def _is_file(record):
    if get_aggregation_level(record) == LEVELS['file']:
        return True

def _is_dataset(record):
    if get_aggregation_level(record) == LEVELS['dataset']:
        return True

def _get_aggregation_field_from_record(record):
    field_string = get_value_from_record(record, KEYS['superpart'])
    if field_string is None:
        field_string = get_value_from_record(record, 'PARENT') # TODO Remove once migration is done!
    return field_string

def _get_list_of_aggregation_handles_from_record(record):
    field_string = get_aggregation_field_from_record(record)
    list_of_strings = field_string.split(SEPARATOR)
    handles = []
    for member in list_of_strings:
        handles.append(_remove_hdl_from_handle(member))
    return handles

def _get_errata_field_from_record(record):
    return get_value_from_record(record, KEYS['errata_ids'])

def _get_errata_list_from_record(record):
    field_string = get_errata_field_from_record(record)
    if field_string is not None:
        return field_string.split(SEPARATOR)
    else:
        return None

def _get_filename_from_record(record):
    return get_value_from_record(record, KEYS['file_name'])

def _get_filesize_from_record(record):
    return get_value_from_record(record, KEYS['file_size'])

def _get_checksum_from_record(record):
    return get_value_from_record(record, KEYS['checksum'])

def _get_checksum_method_from_record(record):
    return get_value_from_record(record, KEYS['checksum_method'])

def _get_urls_original_from_record(record):
    field_string = get_value_from_record(record, KEYS['url_orig'])
    return _parse_urls_original_from_record(field_string)

def _get_urls_replicas_from_record(record):
    field_string = get_value_from_record(record, KEYS['url_replica'])
    return _parse_urls_replicas_from_record(field_string)

# Helpers:

def _parse_urls_original_from_record(field_string):
    parsed = _extract_url_info_from_field(field_string, XML_ATTRIBUTES['href'], XML_ATTRIBUTES['host'])
    if parsed is not None:
        parsed = _rename_href_to_url(parsed)
        parsed = _remove_duplicate_urls(parsed)
    return parsed

def _parse_urls_replicas_from_record(field_string):
    parsed = _extract_url_info_from_field(field_string, XML_ATTRIBUTES['href'], XML_ATTRIBUTES['host'])
    if parsed is not None:
        parsed = _rename_href_to_url(parsed)
        parsed = _remove_duplicate_urls(parsed)
    return parsed

def _rename_href_to_url(list_of_dicts):
    ''' Helper for the list of dict that is the result of parsing
    a locations xml snippet.'''
    for item in list_of_dicts:
        item['url'] = item[XML_ATTRIBUTES['href']]
        del item[XML_ATTRIBUTES['href']]
    return list_of_dicts

def _remove_duplicate_urls(list_of_dicts):
    ''' Helper for the list of dict that is the result of parsing
    a locations xml snippet.
    This removes any item that has an url that was already there,
    no matter of those items differ in other aspects.
    '''
    new_list_of_dicts = []
    temp_list_of_urls = []
    for item in list_of_dicts:
        url = item['url']
        if url not in temp_list_of_urls:
            temp_list_of_urls.append(url)
            new_list_of_dicts.append(item)
    return new_list_of_dicts

def _extract_url_info_from_field(field_string, *attributes):
    if field_string is None:
        return None
    else:
        field_xml = ET.fromstring(field_string)
        list_of_items = []
        locations = field_xml.findall(XML_ATTRIBUTES['location'])
        for location in locations:
            temp = {}
            for attr in attributes:
                val = location.get(attr)
                if val is not None:
                    temp[attr] = val
            list_of_items.append(temp)
        return list_of_items


# Retrieve info on parent dataset from handle system:

def _get_list_of_aggregation_records_from_record(record):
    list_of_aggregation_handles = get_list_of_aggregation_handles_from_record(record)
    list_of_infos, any_replaced = _get_list_of_aggregation_records_from_record(list_of_aggregation_handles)
    return list_of_infos, any_replaced

def _get_list_of_aggregation_records_from_record(list_of_aggregation_handles):
    list_to_be_returned = []
    any_replaced = False

    for handle in list_of_aggregation_handles:
        info, replaced = _extract_info_on_one_aggregation_record(handle)
        list_to_be_returned.append(info)
        if replaced:
            any_replaced = True

    return list_to_be_returned, any_replaced

def _extract_info_on_one_aggregation_record(parenthandle):

    #
    info_to_return = dict(
        handle = parenthandle,
        level = 'level unknown',
        title = '(title unknown)',
        version = 'unknown',
        replaced = False
    )
    replaced = False # We don't know, but need to put something here...
        
    # Retrieve handle:
    try:
        record = retrieval.get_handle_record_json(parenthandle)

        # Get info:
        aggregation_level = get_aggregation_level(record)
        drs_id = get_drs_name_from_record(record)
        vers_num = get_version_number_from_record(record)
        newer_version = get_replaced_by_from_record(record)
        replaced = False
        if newer_version is not None and not newer_version == parenthandle:
            replaced = True

        # Assemble info:
        info_to_return = dict(
            handle = parenthandle,
            level = aggregation_level.lower(),
            title = drs_id,
            version = vers_num,
            replaced = newer_version
        )
    except ValueError:
        pass
    return info_to_return, replaced

###
### Parsing dataset handle records
###

def _get_drs_name_from_record(record):
    return get_value_from_record(record, KEYS['drs_id'])

def _get_version_number_from_record(record):
    return get_value_from_record(record, KEYS['vers_num'])

def _get_replaced_by_from_record(record):
    value = get_value_from_record(record, KEYS['replaced_by'])
    handle = _remove_hdl_from_handle(value)
    return handle

def _get_replaces_from_record(record):
    value = get_value_from_record(record, KEYS['replaces'])
    handle = _remove_hdl_from_handle(value)
    return handle

def _get_hosts_original_from_record(record):
    field_string = get_value_from_record(record, KEYS['data_node'])
    parsed = _extract_url_info_from_field(field_string, XML_ATTRIBUTES['host'])
    return parsed

def _get_hosts_replicas_from_record(record):
    field_string = get_value_from_record(record, KEYS['replica_nodes'])
    parsed = _extract_url_info_from_field(field_string, XML_ATTRIBUTES['host'])
    return parsed

def _get_parts_field_from_record(record):
    field_string = get_value_from_record(record, KEYS['subparts'])
    if field_string is None:
        field_string = get_value_from_record(record, 'CHILDREN') # TODO Remove once migration is done!
    return field_string

def _get_list_of_parts_handles_from_record(record):
    field_string = _get_parts_field_from_record(record)
    list_of_strings = field_string.split(SEPARATOR)
    handles = []
    for string in list_of_strings:
        handles.append(_remove_hdl_from_handle(string))
    return handles

# Retrieve info on child files from handle system:

def _get_list_of_parts_records_from_record(record):
    list_of_parts_handles = _get_list_of_parts_handles_from_record(record)
    list_to_be_returned = []
    any_replaced = False
    for handle in list_of_parts_handles:
        info, replaced = _extract_info_on_one_part_record(handle)
        list_to_be_returned.append(info)
        if replaced:
            any_replaced = True

    return list_to_be_returned, any_replaced

def _extract_info_on_one_part_record(handle):

    # Create variables to be filled by data retrieved from handle
    info_to_return = dict(
        handle = handle,
        level = 'unknown',
        title = '(title unknown)',
        replaced = False
    )
    replaced = False

    # Retrieve handle:
    try:
        record = retrieval.get_handle_record_json(handle)

        # Get info:
        aggregation_level = get_aggregation_level(record)
        title = get_title_from_record(record)
        vers_num = get_version_number_from_record(record)
        newer_version = get_replaced_by_from_record(record)
        if newer_version is not None and not newer_version == handle:
            replaced = True

        # Assemble info:
        info_to_return = dict(
            handle = handle,
            level = aggregation_level.lower(),
            title = title,
            replaced = replaced
        )
        if vers_num is not None:
            info_to_return['version'] = vers_num
    
    except ValueError:
        pass
        

    return info_to_return, replaced

# Parsing helpers

def get_value_from_record(record, key):
    ''' Returns the first occurrence as value.'''
    values = []
    for entry in record['values']:
        if entry['type'] == key:
            value = None
            try:
                value = entry['data']['value']
            except KeyError:
                value = entry['data']
            return value
    return None

def get_values_from_record(record, key):
    ''' Returns all occurrences in a list.'''
    values = []
    for entry in record['values']:
        if entry['type'] == key:
            value = None
            try:
                value = entry['data']['value']
            except KeyError:
                value = entry['data']
            values.append(value)
    return values

def _get_all_entries_as_dict(record, include_special_types=True):
    special_types = DEFAULTS['special_types']
    record_dict = {}
    for entry in record['values']:
        key = entry['type']
        if key in special_types and not include_special_types:
            pass
        else:
            record_dict[key] = str(get_value_from_record(record, key))
    return record_dict

# Handle helper

def _remove_hdl_from_handle(handle):
    if handle is not None:
        if handle.startswith('hdl:'):
            return handle.replace('hdl:','')
        else:
            return handle

