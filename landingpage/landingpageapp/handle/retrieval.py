import json
import requests
from landingpage.landingpageapp.config_and_defaults import CONFIG, DEFAULTS
import logging

LOGGER = logging.getLogger(__name__)
LOGGER.addHandler(logging.NullHandler())

'''
This module retrieves handle records from 
a handle server.
'''


def get_handle_record_json(handle):

    full_url_with_handle = _get_resolve_url_for_handle(handle)
    resp = requests.get(full_url_with_handle+'?auth=True')
    # TODO ONE DAY - MAKE THIS AJAX??? 
    # StackOverflow
    # http://stackoverflow.com/questions/7699796/how-do-you-get-django-to-make-a-restful-call
    # My opinion is to get the client to make the requests where possible; making your webserver block for an external call isn't very scalable or nice in general. However, be aware that AJAX requests must be made to the same domain (you can't do cross domain AJAX requests without using iframes, jsonp, or other trickery). 

    # TODO USE PYHANDLE HERE TO CATCH ERRORS
    record_json = json.loads(resp.content)

    if resp.status_code == 200 and record_json['responseCode'] == 1:
        return record_json
    elif resp.status_code == 404 and record_json['responseCode'] == 100:
        raise ValueError('This handle ('+str(handle)+') does not exist!')
    else:
        raise ValueError('Unknown problem when retrieving handle '+str(handle))

def _get_handle_server_from_config():
    try:
        handleserver = CONFIG['handleserver']
        return handleserver
    except KeyError:
        return None

def _get_global_handle_resolver():
    try:
        val = DEFAULTS['globalresolver']
        return val
    except KeyError:
        msg = 'No default URL for the global handle resolver found.'
        LOGGER.error(msg)
        raise ValueError(msg)

def _get_resolver_base_url():
    handleserver = _get_handle_server_from_config()
    if handleserver is None:
        handleserver = _get_global_handle_resolver()
    return handleserver

def _get_resolver_url():
    base_url = _get_resolver_base_url()
    path = DEFAULTS['path_restapi']
    url =  base_url.strip('/') +'/'+path
    return url

def _get_resolve_url_for_handle(handle):
    if handle.startswith('hdl:'):
        handle = handle.replace('hdl:', '')
    url = _get_resolver_url() + '/'+handle
    return url

if False:

    def check_handle_server_availability():
        host = _get_resolver_base_url()
        resp = Request.blank(host).get_response()

        if resp.status_code == 200:
            return True

        else:
            LOGGER.debug('Server not available, maybe? The call to the handle server at "{host}" returned status code "{code}"!'.format(
                url=host,
                code=resp.status_code
            ))
            return False

    def check_handle_existence(handle):
        handle_server_url = _get_resolve_url_for_handle(handle)
        resp = Request.blank(handle_server_url).get_response()

        if resp.status_code == 200:
            return True

        elif resp.status_code == 404:
            LOGGER.debug('The check for handle existence (handle {handle}), call to "{url}" returned code "404". The handle does not exist.'.format(
                handle=handle,
                url=handle_server_url
            ))
            return False

        else:
            LOGGER.debug('The check for handle existence (handle {handle}), call to "{url}" returned code "{code}", which is strange.'.format(
                handle=handle,
                url=handle_server_url,
                code=resp.status_code
            ))
            return False
