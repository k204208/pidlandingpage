from django.template import loader
import xml.etree.ElementTree as ET
import landingpage.landingpageapp.handle.cmip6parser as parser

#
# API of this module:
# 

def get_context(json_record):
    return _get_context(json_record)
 
def get_template():
    return _get_template()

def _get_template():
    return loader.get_template('landingpage/cmip6_dataset.html')

#
# Logic of this module:
#

def _get_context(json_record):

    context = {}

    context['content'] = json_record
    context['handle'] = json_record['handle']
    context['values'] = parser.get_all_entries_as_dict(json_record)

    # drs
    val = parser.get_drs_name_from_record(json_record)
    if val is not None:
        context['drs'] = val

    # version number
    val = parser.get_version_number_from_record(json_record)
    if val is not None:
        context['version'] = val

    # newer version
    val = parser.get_replaced_by_from_record(json_record)
    if val is not None:
        context['newerversion'] = val

    # older version
    val = parser.get_replaces_from_record(json_record)
    if val is not None:
        context['olderversion'] = val

    # hosts original
    val = parser.get_hosts_original_from_record(json_record)
    if val is not None:
        context['hosts_original'] = val

    # hosts replicas
    val = parser.get_hosts_replicas_from_record(json_record)
    if val is not None:
        context['hosts_replicas'] = val

    # Subparts (formerly known as children)
    val1 = parser.get_list_of_parts_handles_from_record(json_record)
    if val1 is not None:
        context['list_of_parts_handles'] = val1
        val2,val3 = parser.get_list_of_parts_records_from_record(json_record)
        if val2 is not None:
            context['list_of_parts_records'] = val2
            context['any_part_replaced'] = val3

    # Errata
    val = parser.get_errata_list_from_record(json_record)
    if val is not None:
        context['errata'] = val

    return context
