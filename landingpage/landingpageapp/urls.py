from django.conf.urls import url

from . import views

def prepend_start_of_word(regex):
    return r'^'+regex

def append_end_of_word(regex):
    return regex+r'$'

def prepend_hdl(string):
    return prepend_start_of_word(r'[hdl:]*'+string) # Not perfect, also matches hdl:, or hl, or.... TODO Improve.

def append_optional_slash(regex):
    return regex+r'/?'

# generic
suffix_any = r'([\w,-/]+)' # Matches all alphanumeric and comma and minus and slash
# TODO: which chars can be in suffix?

# any handle
prefix_any   = r'([a-zA-Z0-9]+[.]?[a-zA-Z0-9]*)'
prefix_any   = prepend_hdl(prefix_any)
handle_any   = prefix_any + r'/' + suffix_any

# obs4mips
suffix_obs4mips = r'(ESGF/[\w,-/]+)'
prefix_obs4mips = r'(10876)'
prefix_obs4mips = prepend_hdl(prefix_obs4mips)
handle_obs4mips = prefix_obs4mips + r'/'+suffix_obs4mips

#cmip6
prefix_cmip6 = r'(21\.14100)'
prefix_cmip6 = prepend_hdl(prefix_cmip6)
handle_cmip6 = prefix_cmip6 + r'/' + suffix_any

# Now bind urls to views
urlpatterns = [
    url(append_end_of_word(append_optional_slash(prefix_cmip6)),    views.prefix_only_cmip6, name='prefix_only_cmip6'),
    url(append_end_of_word(append_optional_slash(handle_cmip6)),    views.entire_handle_cmip6, name='entire_handle_cmip6'),
    url(append_end_of_word(append_optional_slash(prefix_obs4mips)), views.prefix_only_cmip6, name='prefix_only_cmip6'),
    url(append_end_of_word(append_optional_slash(handle_obs4mips)), views.entire_handle_cmip6, name='entire_handle_cmip6'),
    url(append_end_of_word(append_optional_slash(handle_any)),      views.entire_handle, name='entire_handle'),
    url(append_end_of_word(append_optional_slash(prefix_any)),      views.prefix_only, name='prefix_only'),
    url(r'$', views.no_handle, name='no_handle'),
]

