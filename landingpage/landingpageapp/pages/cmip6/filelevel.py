from django.template import loader
import xml.etree.ElementTree as ET
import landingpage.landingpageapp.handle.cmip6parser as parser

#
# API of this module:
# 

def get_context(json_record):
    return _get_context(json_record)
 
def get_template():
    return _get_template()

def _get_template():
    return loader.get_template('landingpage/cmip6_file.html')

#
# Logic of this module:
#

def _get_context(json_record):

    context = {}

    context['content'] = json_record
    context['handle'] = json_record['handle']
    context['values'] = parser.get_all_entries_as_dict(json_record)

    # file name
    val = parser.get_filename_from_record(json_record)
    if val is not None:
        context['filename'] = val

    # file size
    val = parser.get_filesize_from_record(json_record)
    if val is not None:
        context['filesize'] = val

    # checksum
    val = parser.get_checksum_from_record(json_record)
    if val is not None:
        context['checksum'] = val

    # checksum method
    val = parser.get_checksum_method_from_record(json_record)
    if val is not None:
        context['checksum_method'] = val

    # file size
    val = parser.get_filesize_from_record(json_record)
    if val is not None:
        context['filesize'] = val

    # URLS original
    val = parser.get_urls_original_from_record(json_record)
    if val is not None:
        context['urls_original'] = val

    # URLS replicas
    val = parser.get_urls_replicas_from_record(json_record)
    if val is not None:
        context['urls_replicas'] = val

    # Aggregation (formerly known as parent)
    val1 = parser.get_list_of_aggregation_handles_from_record(json_record)
    if val1 is not None:
        context['list_of_aggregation_handles'] = val1
        val2,val3 = parser.get_list_of_aggregation_records_from_record(val1)
        if val2 is not None:
            context['list_of_aggregation_records'] = val2
            context['any_parent_replaced'] = val3

    return context
